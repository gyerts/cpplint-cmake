This script has possibility to prohibit commit into git local repo, and makes cmake build errors when new lints was detected.

First build a priori can't has an errors
Second and next builds will compare created reports with old one
If new report has no errors (or some errors was deleted) old report will rewritten with new one report. 

```
#!bash
clone repository somewhere
cd somewhere
rm .git/hooks/pre-commit.sample
cp pre-commit .git/hooks/pre-commit
sudo apt-get install cmake
cd ..
cmake somewhere/
```
Add some changes into main.cpp file (add wrong google style lines) eg:
```
#!cpp
int f(){
return; }
```
```
#!bash
cmake somewhere/   (you should see error)
cd somewhere/
git add main.cpp
git commit -m "you haven't possibility to commit this changes"   (you should see error)
```