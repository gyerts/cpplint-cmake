#!/usr/bin/python

import os
import re
import shutil
import xml.etree.ElementTree as ET


class SourceFile:
	def __init__(self, path):
		self.path = path
		self.lints = []

	def add_lint(self, lint):
		self.lints.append(lint)

	def __sub__(self, right):
		new_file = SourceFile(self.path)
		old_lints = self.lints[:]

		for lint in right.lints:
			try:
  				old_lints.remove(lint)
  			except:
  				print("   -   success! (-1) cpplint: {}".format(lint))

  		for lint in old_lints:
  			new_file.add_lint(lint)

		return new_file

	def __str__(self):
		return str([self.path, self.lints]) 

class Report:
	def __init__(self):
		self.files = {}

	def add_file(self, file):
		self.files[file.path] = file

	def has_files(self):
		return bool(len(self.files))

	def __contains__(self, item):
		return item.path in self.files		

	def __getitem__(self, item):
		return self.files[item.path]

	def __str__(self):
		ans = str()

		for file in self.files:
			ans += self.files[file].path + "\n"
			for lint in self.files[file].lints:
				ans += "    " + lint + "\n"
		return ans

	def __trunc__(self):
		quantity = 0

		for file in self.files:
			quantity += len(self.files[file].lints)

		return quantity

	def __sub__(self, other_report):
		new_report = Report()
		
		for file in self:
			if(file in other_report):
				diff_file = file - other_report[file]
				if len(diff_file.lints):
					new_report.add_file(diff_file)

		for self_file in self:
			if self_file not in other_report:
				new_report.add_file(self_file)

		return new_report

	def __iter__(self):
		return (self.files[file] for file in self.files)

def regex_message(message):
	regex = r"^[\d]+: ([^^]+)"
	matches = re.finditer(regex, message)

	for matchNum, match in enumerate(matches):     
	    return match.group(1)

def get_files_with_lints(path_to_xml_file_with_junit_output):
	tree = ET.parse(path_to_xml_file_with_junit_output)
	root = tree.getroot()

	report = Report()

	for file in root.findall('testcase'):
		new_file = SourceFile(file.get("name"))

		for failure in file.findall('failure'):
			message = failure.get("message")
			new_file.add_lint(regex_message(message))
		report.add_file(new_file)

	return report


new_report_path = "/home/gyerts/Desktop/new.xml"
old_report_path = "/home/gyerts/Desktop/old.xml"
diff_report_path = "/home/gyerts/Desktop/diff.txt"


if(os.path.exists(old_report_path)):
	old_report = get_files_with_lints(old_report_path)
	new_report = get_files_with_lints(new_report_path)

	diff_report = new_report - old_report

	if diff_report.has_files():
		count_of_lints = int(diff_report)
		if(count_of_lints > 1):
			print("error: you have {} new google style lints! Commit prohibited!".format(count_of_lints))
		else:
			print("error: you have {} new google style lint! Commit prohibited!".format(count_of_lints))
		print("-- Please go and compare 3 files")
		print("    * " + new_report_path)
		print("    * " + old_report_path)
		print("    * " + diff_report_path)
		with open(diff_report_path, "w") as f:
			f.write(str(diff_report))
		exit(1)
	else:
		shutil.copy(new_report_path, old_report_path)
else:
	shutil.copy(new_report_path, old_report_path)
	exit(0)
